To run the application you need to compile the repo using maven.

Run the following command in the current directory:

```
mvn clean package.
```

This should create a 'target' directory in which the 'jar/war' file exits (depending on the pom.xml configurations).

Next run the following command:

```
mvn spring-boot:run
```

This should run the application on http://localhost:8080/

Also, I wrote some code that generates a list (arraylist) of employees on the server that is then rendered on the front end using thymeleaf. 

The code is on a seperate branch 'trial/array-list"

To get to it, type in the following command:

```
git checkout trial/array-list
```
